(define-module (alexgb0 packages alexgb0-dwm)
  #:use-module (gnu packages suckless)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (alexgb0 packages))

(define-public alex-dwm
  (package
    (inherit dwm)
    (name "alex-dwm")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
          (url "https://gitlab.com/alexgb0/dwm.git")
          (commit "36cdff8df060365466eac7526f7cd4ed8db0f686")))
        (sha256
          (base32 "1f4yidapr4i7w38dwak51ayda2m5pxgm9xq093c3fr3mkvxy45n3")))) ; I have no fucking idea how I got this hash man
    (home-page "https://gitlab.com/alexgb0/dwm")
    (synopsis "alexgb0's version of dwm")))